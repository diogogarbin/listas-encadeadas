<h1 align="center">Listas Simplesmente encadeadas e Duplamente encadedas</h1>



<h2> Lista Duplamente encadeada </h2>

<ul>
	<li> Uma lista simplesmente encadeada é uma sequência de
	nós, onde cada nó contém uma informação de algum tipo de
	dado e o endereço do nó seguinte onde neste tipo de lista,
	podemos percorrer apenas por um sentido, sendo ele da cabeça
	da lista. Mais eficiente quato ao consumo de memoria. </li>
	<li> Exemplo pratico de aplicabilidade seria por exemplo em uma
	loja ou algo que envolvesse senhas sequenciais, onde cada nó chamaria 
	o proximo elemento da lista, sendo ela a proxima senha.   </li>
	
</ul>	

![alt text](https://www.ime.usp.br/~pf/algoritmos/aulas/png-from-tex/lista.png)

<h2> Lista Duplamente encadeada! </h2>

<ul>
	<li>Lista Duplamente Encadeada É um tipo de lista encadeada que pode 
	ser vazia ou que pode ter um ou mais nós, sendo que cada nó possui 
	dois ponteiros: um que aponta para o nó anterior e outro que aponta 
	para o próximo nó. Ela costuma na pratica ter melhor performance, porem
	demanda um consumo maior de memoria. </li>
	<li> Exemplo pratico de aplicabilidade seria onde precisariamos ter valores em
	uma lista e durante uso haver a necessidade de precisar de um valor prioritario,
	assim percorre-se do começou ou do fim da lista para econtrar este valor. </li>	
</ul>		

![alt text](https://cdn.discordapp.com/attachments/691778925294256241/836752270204731452/1.png)	







