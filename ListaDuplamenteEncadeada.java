public class Vagao {
    String nomeDoVagao;
    Wagon vagaoAnterior;
    Wagon vagaoPosterior;

    public Vagao (String nomeDoVagao){
        this.nomeDoVagao = nomeDoVagao;
        vagaoAnterior = null;
        vagaoPosterior = null;
    }
}

public class Trem {
    Vagao head, tail = null;
    public void adicionarVagao (Vagao vagaoCriar){
        Vagao novo_Vagao = new Vagao (vagaoCriar.nomeDoVagão);

        if (head == null){
            head = tail = novo_Vagao;
            head.vagaoAnterior = null;
            tail.vagaoPosterior = null;
        } else {
            tail.vagaoPosterior = novo_Vagao;
            novo_Vagao.vagaoAnterior = tail;
            tail = novo_Vagao;
            tail.vagaoPosterior = null;
        }
    }
}